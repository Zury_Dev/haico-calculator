/* Aplication name : Haico Calculator */
/* Developer: Ezequiel Henrique */
/* Language: C */
/*Level : Beginner*/

#include <stdio.h>
#include <math.h>

/*Essencial functions*/
double som(double a, double b)
{
    return a+b;
}
double sub(double a, double b)
{
    return a-b;
}
double mul(double a, double b)
{
    return a*b;
}
double div(double a, double b)
{
    return a/b;
}

/*declaration - necessary functions */
int system();
int sleep();
double sqrt();


int main(int argc , char *argv[])
{
    /*Variables area*/
    double n1,n2,sq;
    char op;

    printf("Welcome to Haico Calculator\n");
    sleep(3);
    system("clear || cls");
    printf("enter a operation: [  +  -  x  /  s(for squareroot) ] \n");
    scanf("%c", &op);
    printf("Enter a number:\n");
    scanf("%lf", &n1);
    if(op != 's')
    {
    printf("Enter a number:\n");
    scanf("%lf", &n2);
    }
    /*Decision structure*/
    switch(op)
    {
    case '+':
        printf("The result is: %lf\n", som(n1,n2));
        break;
    case '-':
        printf("The result is: %lf\n", sub(n1,n2));
        break;
    case '*':
        printf("The result is: %lf\n", mul(n1,n2));
        break;
    case '/':
        printf("The result is: %lf\n", div(n1,n2));
        break;
    case 's':
        sq = sqrt(n1);
        printf("The result is: %lf\n", sq);
        break;
    default:
        printf("Operator Error, try again...\n");
        sleep(1);
        printf("Operator Error, try again...\n");
        sleep(1);
        printf("Operator Error, try again...\n");
        sleep(1);
        break;
    }

    return 0;
}

